﻿using System;
namespace CustomFramework.EventBus.Sample
{
    public struct PressedButtonEvent
    {
        public DateTime PressTime;
        public Button_ID Button_ID;
    }

    public enum Button_ID
    {
        RED,
        BLUE,
        GREEN,
    }
}
