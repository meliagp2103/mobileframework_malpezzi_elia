﻿using UnityEngine;
using CustomFramework.ServiceLocator;
namespace CustomFramework.EventBus.Sample
{
    public class SampleEventRegister : ARegister
    {
        private EventBusSystem _eventBus;

        public override void Register()
        {
            _eventBus = ServiceLocatorSystem.GetOrCreateSystem<EventBusSystem>().FromSceneLocator();
            _eventBus.DeclareEvent<PressedButtonEvent>();
        }
    }
}

