﻿using CustomFramework.Utils;
using UnityEngine;
using TMPro;

namespace CustomFramework.Managers.Sample
{
    public class SampleInputManager : AInputManager
    {
        private CameraRaycasterSystem cameraRaycasterSystem;
        public TextMeshProUGUI console;
        private void Awake()
        {
            cameraRaycasterSystem = new CameraRaycasterSystem();
        }

        protected override void OnGesture(GestureInfo debuginfo)
        {
            switch (debuginfo.Gesture)
            {
                case Gesture.Tap:
                    var hitObject = cameraRaycasterSystem.GetRaycastFromCameraToWorld(PointerPosition).collider.gameObject;
                    console.text = $"hit : {hitObject}";
                    break;
                case Gesture.DoubleTap:
                    break;
                case Gesture.Hold:
                    break;
                case Gesture.Swipe:
                    break;
                case Gesture.Drag:
                    break;
                case Gesture.Pinch:
                    break;
            }
        }
    }
}