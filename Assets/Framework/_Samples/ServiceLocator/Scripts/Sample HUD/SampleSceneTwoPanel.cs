﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CustomFramework.ServiceLocator.Sample
{
    public class SampleSceneTwoPanel : MonoBehaviour
    {
        private SampleSceneHandlerSystem _sceneHandlerSystem;
        private SampleScoreHandlerSystem _scoreHandlerSystem;

        private Text _text;

        private void Awake()
        {
            _sceneHandlerSystem = ServiceLocatorSystem
                .GetOrCreateSystem<SampleSceneHandlerSystem>()
                .FromProjectLocator();

            _scoreHandlerSystem = ServiceLocatorSystem
                .GetOrCreateSystem<SampleScoreHandlerSystem>()
                .FromProjectLocator();

            _text = ServiceLocatorSystem
                .ResolveDependecy<Text>()
                .TakeOne("score_text");

            _text.text = $"Score: {_scoreHandlerSystem.Score}";
        }

        public void OnClick() => _sceneHandlerSystem.LoadMainScene();
    }
}

