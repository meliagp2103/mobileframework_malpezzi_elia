﻿using System.Collections.Generic;
using UnityEngine;

namespace CustomFramework.ServiceLocator.Sample
{
    public class SampleLocatorUser : MonoBehaviour
    {
        SampleLocatorRegistererComponent _sampleLocatorRegistererComponent;
        List<ISampleInterface> _sampleInterfaceUserItems;

        private void Awake()
        {
            _sampleLocatorRegistererComponent = ServiceLocatorSystem.ResolveDependecy<SampleLocatorRegistererComponent>()
                .TakeOne("test");

            _sampleInterfaceUserItems = ServiceLocatorSystem.ResolveDependecy<ISampleInterface>().GetAll();
            
            
        }
        private void Start()
        {
            _sampleLocatorRegistererComponent.SampleDebug();
            _sampleInterfaceUserItems.ForEach(user => user.Sample());
        }
    }

}
