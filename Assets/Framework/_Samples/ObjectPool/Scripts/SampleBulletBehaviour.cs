﻿using UnityEngine;

namespace CustomFramework.Managers.Sample
{
    public class SampleBulletBehaviour : MonoBehaviour, IPoolable<Transform, IMemoryPool>
    {
        private IMemoryPool _pool;

        private void OnTriggerEnter(Collider other)
        {
            _pool?.Despawn(this);
        }

        public void OnDespawn() // resests all params
        {
            _pool = null;
        }

        private void Update()
        {
            transform.Translate(transform.forward * 20 * Time.deltaTime);
        }
        public void OnSpawn(Transform param1, IMemoryPool param2)
        {
            transform.position = param1.position;
            transform.rotation = param1.rotation;

            _pool = param2;
        }

        public void OnCreate() { }
    }
}

