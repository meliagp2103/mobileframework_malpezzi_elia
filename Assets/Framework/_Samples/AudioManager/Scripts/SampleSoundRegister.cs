﻿using CustomFramework.ServiceLocator;
using UnityEngine;

namespace CustomFramework.Managers.Sample
{
    public class SampleSoundRegister : ARegister
    {
        public override void Register()
        {
            var audioClipContainer = Resources.Load<AudioClipContainer>("Audio Clip Container");
            ServiceLocatorSystem
                .Register<AudioClipContainer>(audioClipContainer)
                .WithID("scene_audio_clips")
                .AsSceneAccess();
        }
    }
}

