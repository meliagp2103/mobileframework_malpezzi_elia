﻿using CustomFramework.ServiceLocator;
using CustomFramework.UI;
using UnityEngine;

namespace CustomFramework.Managers.Sample
{
    public class SampleSoundUser : MonoBehaviour
    {
        SampleSoundManagerSystem _soundManager;
        ButtonHandler _playClipSampleOneButton;
        private void Awake()
        {
            _soundManager = ServiceLocatorSystem
                .GetOrCreateSystem<SampleSoundManagerSystem>()
                .FromSceneLocator();

            _playClipSampleOneButton = ServiceLocatorSystem
                .ResolveDependecy<ButtonHandler>()
                .TakeOne("clip_one_button");
        }

        private void Start()
        {
            _playClipSampleOneButton.SetButtonAction(PlaySampleOneClip);
            _playClipSampleOneButton.SetButtonText("sample button player");
        }
        private void PlaySampleOneClip()
        {
            _soundManager.PlayOneShotSound("sample_one");
        }

        private void Update()
        {
            //if (Input.GetKeyDown(KeyCode.Alpha1))
            //   

            if (Input.GetKeyDown(KeyCode.Alpha2))
                _soundManager.PlayOneShotSound("sample_two");

            if (Input.GetKeyDown(KeyCode.Alpha3))
                _soundManager.PlayOneShotSound("sample_three");
        }
    }
}

