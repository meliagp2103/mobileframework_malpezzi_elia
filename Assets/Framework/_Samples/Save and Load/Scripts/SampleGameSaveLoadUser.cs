﻿using CustomFramework.ServiceLocator;
using CustomFramework.UI;
using CustomFramework.Utils;
using UnityEngine;

namespace CustomFramework.Managers.Sample
{
    public class SampleGameSaveLoadUser : MonoBehaviour, ISavable
    {
        private SampleGameData _sampleGameData;
        private ButtonHandler _scoreUpButton;

        private void Awake()
        {
            _scoreUpButton = ServiceLocatorSystem
                .ResolveDependecy<ButtonHandler>()
                .TakeOne("sample_button");
            _scoreUpButton.SetButtonAction(ScoreUp);
        }
        private void Start()
        {
            updateButtonText();
        }
        public void ScoreUp()
        {
            _sampleGameData.Score++;
            updateButtonText();
        }

        private void updateButtonText()
        {
            _scoreUpButton?.SetButtonText($"{_sampleGameData.Score}");
        }

        public void Load()
        {
            _sampleGameData =  FileStreamUtility.ReadBinary<SampleGameData>(SampleGameData.saveName);
            updateButtonText();
        }
        public void Save()
        {
            FileStreamUtility.WriteBinary(_sampleGameData, SampleGameData.saveName);
        }

        public void ResetData()
        {
            FileStreamUtility.ResetFile<SampleGameData>(SampleGameData.saveName);
        }
    }
}

