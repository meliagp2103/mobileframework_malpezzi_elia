﻿using CustomFramework.ServiceLocator;
using CustomFramework.UI;
using UnityEngine;

namespace CustomFramework.Managers.Sample
{
    public class SampleSaveUIContoller : MonoBehaviour
    {
        private ButtonHandler _saveButton;
        private ButtonHandler _loadButton;
        private ButtonHandler _resetDataButton;

        private void Awake()
        {
            var saveManager = ServiceLocatorSystem
                .GetOrCreateSystem<SampleSaveManagerSystem>()
                .FromSceneLocator();

            _saveButton = ServiceLocatorSystem
                .ResolveDependecy<ButtonHandler>()
                .TakeOne("sample_button_save");

            _loadButton = ServiceLocatorSystem
                .ResolveDependecy<ButtonHandler>()
                .TakeOne("sample_button_load");

            _resetDataButton = ServiceLocatorSystem
                .ResolveDependecy<ButtonHandler>()
                .TakeOne();

            _loadButton.SetButtonAction(saveManager.LoadAll);
            _saveButton.SetButtonAction(saveManager.SaveAll);
            _resetDataButton.SetButtonAction(saveManager.WipeData);
        }
    }
}

