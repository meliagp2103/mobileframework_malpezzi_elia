﻿using CustomFramework.ServiceLocator;

namespace CustomFramework.Managers
{
    public class SampleSaveManagerSystem : ASaveAndLoadManagerSystem
    {
        public SampleSaveManagerSystem()
        {
            _savableList = ServiceLocatorSystem
                .ResolveDependecy<ISavable>()
                .GetAll();

            LoadAll();
        }
    }
}


