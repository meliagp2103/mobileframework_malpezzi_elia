﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace CustomFramework.Utils
{
    public static class FileStreamUtility
    {
        public static void WriteBinary<T>(T data, string saveName) where T : class, new()
        {
            string completePath = $"{Application.persistentDataPath}/{saveName}.data";

            var jsonFile = JsonUtility.ToJson(data);
                using (var stream = new FileStream(completePath, FileMode.OpenOrCreate))
                {
                    var binaryFormatter = new BinaryFormatter();
                    binaryFormatter.Serialize(stream, jsonFile);
                }
        }

        public static T ReadBinary<T>(string saveName) where T : class, new()
        {
            string completePath = $"{Application.persistentDataPath}/{saveName}.data";

            if (File.Exists(completePath))
            {

                using (var stream = new FileStream(completePath, FileMode.Open))
                {
                    var formatter = new BinaryFormatter();
                    string jsonFile = formatter.Deserialize(stream) as string;
                    return JsonUtility.FromJson<T>(jsonFile);
                }
            }
            return new T();
        }

        public static void ResetFile<T>(string saveName) where T : class, new()
        {
            WriteBinary(new T(), saveName);
        }

    }

}
