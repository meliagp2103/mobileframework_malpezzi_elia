﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CustomFramework.ServiceLocator
{
    public class ProjectLocator : ALocator
    {
        protected override void Awake()
        {
            DontDestroyOnLoad(this.gameObject);
            base.Awake();
        }
    }
}
