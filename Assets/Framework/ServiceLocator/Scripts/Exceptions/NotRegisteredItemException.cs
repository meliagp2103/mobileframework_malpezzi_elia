﻿using System;
namespace CustomFramework.ServiceLocator.Exceptions
{
    public class NotRegisteredItemException : Exception
    {
        public NotRegisteredItemException(string message) : base(message)
        {
        }
    }
}

