﻿using System;
namespace CustomFramework.ServiceLocator.Exceptions
{
    public class AccessingToNullLocatorException : Exception
    {
        public AccessingToNullLocatorException(string message) : base(message)
        {
        }
    }
}

