﻿using CustomFramework.ServiceLocator.Exceptions;
using System.Collections.Generic;
using System.Linq;

namespace CustomFramework.ServiceLocator
{
    public static class ResolveExtension
    {
        public static List<T> GetAll<T>(this List<(T item, string id)> itemList)
        {
            if (itemList.Count == 0)
                throw new NotRegisteredItemException($"SERVICE LOCATOR: no item of type {typeof(T)} is registered");

            return itemList
                .Select(item => item.item)
                .ToList();

        }

        public static T TakeOne<T>(this List<(T item, string id)> itemList, string id = "")
        {
            var itemsWithID = itemList.
                    Where(obj => obj.id == id);

            switch (itemsWithID.Count())
            {
                case int c when c == 0:
                    throw new NotRegisteredItemException($"SERVICE LOCATOR: item of type {typeof(T)} with id \"{id}\" not found");
                case int c when c > 1:
                    throw new MultipleItemIDMatchingException($"SERVICE LOCATOR: multiple registration of {typeof(T)} with id \"{id}\" this is not allowed ");
                default:
                    return itemsWithID.First().item;

            }
        }
    }
}
