﻿using CustomFramework.ServiceLocator.Exceptions;
using UnityEngine;

namespace CustomFramework.ServiceLocator
{
    public static class RegistrationExtension
    {
        public static Registration WithID(this Registration item, string id)
        {
            item.ID = id;
            return item;
        }

        public static void AsProjectAccess(this Registration item)
        {
            if (!ServiceLocatorSystem.ProjectLocator)
                throw new AccessingToNullLocatorException("SERVICE LOCATOR: Project Locator not instanced");

            ServiceLocatorSystem.ProjectLocator.AddRegistration(item);
        }

        public static void AsSceneAccess(this Registration item)
        {
            if (!ServiceLocatorSystem.SceneLocator)
                throw new AccessingToNullLocatorException("SERVICE LOCATOR: Scene Locator not instanced");

            ServiceLocatorSystem.SceneLocator.AddRegistration(item);
        }
    }
}
