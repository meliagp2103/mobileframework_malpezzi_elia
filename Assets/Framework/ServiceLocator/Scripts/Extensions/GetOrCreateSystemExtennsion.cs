﻿using CustomFramework.ServiceLocator.Exceptions;
using System.Linq;

namespace CustomFramework.ServiceLocator
{
    public static class GetOrCreateSystemExtennsion
    {
        public static Research<T> WithID<T>(this Research<T> research, string id)
        {
            research.id = id;
            return research;
        }

        public static T FromProjectLocator<T>(this Research<T> research) where T : class, new()
        {
            if (!ServiceLocatorSystem.ProjectLocator)
                throw new AccessingToNullLocatorException($"SERVICE LOCATOR: trying create or get as system {typeof(T)} on not instantiate project locator");

            var itemList = ServiceLocatorSystem.ProjectLocator
                .GetItemList<T>()
                .Where(item => item.id == research.id);

            if (itemList.Count() > 0)
                return itemList.First().item;

            var registration = new Registration<T>(new T()) { ID = research.id };
            ServiceLocatorSystem.ProjectLocator.AddRegistration(registration);
            return registration.Item as T;
        }

        public static T FromSceneLocator<T>(this Research<T> research) where T : class, new()
        {
            if (!ServiceLocatorSystem.SceneLocator)
                throw new AccessingToNullLocatorException($"SERVICE LOCATOR: trying create or get as system {typeof(T)} on not instantiate scene locator");
            var itemList = ServiceLocatorSystem.SceneLocator
                .GetItemList<T>()
                .Where(item => item.id == research.id);

            if (itemList.Count() > 0)
                return itemList.First().item;

            var registration = new Registration<T>(new T()) { ID = research.id };
            ServiceLocatorSystem.SceneLocator.AddRegistration(registration);
            return registration.Item as T;
        }
    }
}
