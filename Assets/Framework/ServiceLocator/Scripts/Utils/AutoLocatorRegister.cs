﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace CustomFramework.ServiceLocator
{
    public class AutoLocatorRegister : MonoBehaviour
    {
#pragma warning disable 649
        [SerializeField] private RegisterContext _context;
        [SerializeField] private string ID;
        [SerializeField] private List<Component> components;
#pragma warning restore 649

        private void Awake()
        {
            var registrations = components
                .Select(component => ServiceLocatorSystem.Register(component).WithID(ID)).ToList();

            switch (_context)
            {
                case RegisterContext.Scene:
                    registrations.ForEach(reg => reg.AsSceneAccess());
                    break;
                case RegisterContext.Project:
                    registrations.ForEach(reg => reg.AsProjectAccess());
                    break;
            }
        }
    }
    public enum RegisterContext
    {
        Scene,
        Project,
    }
}
