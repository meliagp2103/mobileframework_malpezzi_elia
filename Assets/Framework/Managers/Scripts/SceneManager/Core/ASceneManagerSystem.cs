﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace CustomFramework.Managers
{
    public abstract class ASceneManagerBaseSystem
    {
        public virtual void LoadLauncher() => SceneManager.LoadScene(0);
        public virtual void LoadGameplay() => SceneManager.LoadScene(1);
        public virtual void Quit() => Application.Quit();
    }

    public abstract class ASceneManagerEndgameSceneSystem : ASceneManagerBaseSystem
    {
        public virtual void LoadEndGame() => SceneManager.LoadScene(2);
    }
}

