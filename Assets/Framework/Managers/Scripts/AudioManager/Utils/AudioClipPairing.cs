﻿using UnityEngine;

namespace CustomFramework.Managers
{
    [System.Serializable]
    public struct AudioClipPairing
    {
        public AudioClip Clip;
        public string Identifier;
    }
}

