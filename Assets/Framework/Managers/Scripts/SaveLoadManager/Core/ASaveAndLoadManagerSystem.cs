﻿
using CustomFramework.ServiceLocator;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomFramework.Managers
{
    public abstract class ASaveAndLoadManagerSystem
    {
        protected List<ISavable> _savableList;

        public virtual void SaveAll()
        {
            foreach (var savable in _savableList)
                savable.Save();
        }

        public virtual void LoadAll()
        {
            foreach (var savable in _savableList)
                savable.Load();
        }
        public virtual void WipeData()
        {
            foreach (var savable in _savableList)
                savable.ResetData();
            LoadAll();
        }
    }
}


