﻿namespace CustomFramework.Managers
{
    public interface IMemoryPool
    {
        void Despawn(object obj);
    }

    public interface IMemoryPool<T> 
        where T : IPoolable<IMemoryPool>
    {
        T Spawn();
    }

    public interface IMemoryPool<T1, T2>
        where T2 : IPoolable<T1, IMemoryPool>
    {
        T2 Spawn(T1 param1);
    }

    public interface IMemoryPool<T1, T2, T3> 
        where T3 : IPoolable<T1, T2, IMemoryPool>
    {
        T3 Spawn(T1 param1, T2 param2);
    }
}

