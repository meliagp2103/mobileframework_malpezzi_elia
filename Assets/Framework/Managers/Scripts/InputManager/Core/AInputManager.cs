﻿using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;

namespace CustomFramework.Managers
{
    public abstract class AInputManager : MonoBehaviour
    {
        private Gesture _gesture = Gesture.NotProcessed;
        public Gesture Gesture
        {
            get => _gesture;
            set
            {
                if (value == Gesture.NotProcessed && _gesture == Gesture.NotProcessed) return;

                var gesturephase = value == Gesture.NotProcessed ? GesturePhase.End : GesturePhase.Starting;
                var gesture = gesturephase == GesturePhase.Starting ? value : _gesture;

                var gestureInfo = new GestureInfo()
                {
                    Gesture = gesture,
                    GesturePhase = gesturephase
                };

                OnGesture(gestureInfo);
                _gesture = value;

            }
        }

        public float _gestureProcessWindowTime = 0.2f;
        public float _swipeWindowTime = 0.2f;
        public float _deadzone = 100f;

        public static Vector2 PointerPosition { get; private set; }
        public  static Vector2 PointerStartPosition { get; private set; }
        public  static Vector2 PointerDeltaFromStart => PointerStartPosition - PointerPosition;
        public  static Vector2 PointerDoubleTouchDinstance { get; protected set; }

        public float StartTapTime { get; protected set; }
        public float InputDuration => Time.time - StartTapTime;


        private bool _isPressing = false;
        private bool _isProcessingStationaryTap = false;
        private bool IsDeadzoneExceed => PointerDeltaFromStart.sqrMagnitude > Mathf.Pow(_deadzone, 2);
         
        protected virtual void OnGesture(GestureInfo gestureInfo)
        {

        }

        public void OnTap(InputAction.CallbackContext context)
        {
            if (context.started)
            {
                _gesture = Gesture.NotProcessed;
                _isPressing = true;
                StartTapTime = Time.time;

                StartCoroutine(ProcessMovements());

                if (!_isProcessingStationaryTap)
                    StartCoroutine(ProcessStationaryTap());
            }

            if(context.canceled)
            {
                _isPressing = false;
                Gesture = Gesture.NotProcessed;
            }
        }

        public void OnDoubleTap(InputAction.CallbackContext context)
        {
            if (context.performed && Gesture == Gesture.NotProcessed)
                Gesture = Gesture.DoubleTap;
        }

        public void OnHold(InputAction.CallbackContext context)
        {
            if (context.performed && Gesture == Gesture.NotProcessed)
                Gesture = Gesture.Hold;
        }

        public void UpdatePointerPosition(InputAction.CallbackContext context)
        {
            PointerPosition = context.ReadValue<Vector2>();
        }

        public void OnPinch(InputAction.CallbackContext context)
        {
            PointerDoubleTouchDinstance = context.ReadValue<Vector2>();
            if(Gesture == Gesture.NotProcessed)
                Gesture = Gesture.Pinch;
        }

        private IEnumerator ProcessMovements()
        {

            yield return null;
            PointerStartPosition = PointerPosition;

            while(_isPressing && (_gesture == Gesture.NotProcessed || _gesture == Gesture.Drag))
            {
                if (Gesture == Gesture.NotProcessed && InputDuration >= _swipeWindowTime && IsDeadzoneExceed)
                    Gesture = Gesture.Drag;
                yield return null;
            }

            if(Gesture == Gesture.NotProcessed && InputDuration < _swipeWindowTime && IsDeadzoneExceed)
            {
                Gesture = Gesture.Swipe;
            }
        }

        private IEnumerator ProcessStationaryTap()
        {
            _isProcessingStationaryTap = true;
            var startTime = Time.time;
            while(Time.time - startTime < _gestureProcessWindowTime)
                yield return null;

            if (Gesture == Gesture.NotProcessed && !_isPressing)
                Gesture = Gesture.Tap;

            _isProcessingStationaryTap = false;
        }
    }
}

