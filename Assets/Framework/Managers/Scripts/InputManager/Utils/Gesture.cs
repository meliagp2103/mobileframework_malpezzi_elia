﻿namespace CustomFramework.Managers
{
    public enum Gesture
    {
        NotProcessed,
        Tap,
        DoubleTap,
        Hold,
        Swipe,
        Drag,
        Pinch,
    }
}

