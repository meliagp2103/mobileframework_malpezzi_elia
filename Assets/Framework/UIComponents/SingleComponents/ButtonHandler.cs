﻿using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace CustomFramework.UI
{
    public class ButtonHandler : MonoBehaviour
    {
        private Action _action;
        private Button _button;
        private TextMeshProUGUI _buttonText;

        public string ButtonText => _buttonText.text;

        private void Awake()
        {
            _button = GetComponent<Button>();
            _buttonText = GetComponentInChildren<TextMeshProUGUI>();

            _button.onClick.AddListener(OnClickAction);
        }
        private void OnDestroy() => _button.onClick.RemoveAllListeners();

        private void OnClickAction() => _action?.Invoke();

        public void SetButtonAction(Action action) => _action = action;
        public void SetButtonText(string text) => _buttonText.text = text;
    }
}

